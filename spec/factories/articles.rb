FactoryBot.define do
  factory :article do
    author "MyString"
    description "MyText"
    name "MyString"
    title "MyString"
    url "MyString"
    url_to_image "MyString"
  end
end
