class ArticlesController < ApplicationController
  before_action :authenticate_user!

  def index
    @articles = current_user.articles
  end

  def create
    article = current_user.articles.create(article_params)
    flash[:notice] = 'The article was successfully saved!'
    redirect_to root_path
  end

  private

  def article_params
    params.permit(:title, :description, :url, :url_to_image, )
  end
end
