require 'google/cloud/translate'

class VisitorsController < ApplicationController
  def index
    @translate = Google::Cloud::Translate.new(
      key: Rails.application.secrets.google_api_key
    )

    newsapi = News.new("6c14c7eb0c254e21b193400d26e8a4e1")
    query = params[:query] || 'south africa'
    @articles = newsapi.get_everything(
      q: query,
      from: 1.week.ago.strftime('%Y-%m-%d'),
      to: Date.today.strftime('%Y-%m-%d'),
      language: 'en',
    )
  end
end
