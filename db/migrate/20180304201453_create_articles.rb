class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :author
      t.text :description
      t.string :name
      t.string :title
      t.string :url
      t.string :url_to_image

      t.timestamps
    end
  end
end
